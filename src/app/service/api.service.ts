import { Injectable } from "@angular/core";
import { Persona } from "../model/persona";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  
  private url = "localhost:8080/persona/personas";

  constructor(private http: HttpClient) {}

  public getPersonas(): Observable<Persona[]> {
    return this.http.get<Persona[]>(this.url);
  }
}