import { Component, OnInit } from '@angular/core';
import { Persona } from '../model/persona';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-consultar-personas',
  templateUrl: './consultar-personas.component.html',
  styleUrls: ['./consultar-personas.component.css']
})
export class ConsultarPersonasComponent implements OnInit {

  apiservice: ApiService;
  personas: Persona[];
  constructor(apiservice: ApiService) {}

  getPersonas(){
    this.apiservice.getPersonas().subscribe( personas =>{this.personas = personas});
    
  }

  ngOnInit() {
    this.getPersonas();
  }
}
