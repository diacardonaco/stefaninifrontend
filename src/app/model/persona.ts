export interface  Persona {
    codigo: number;
    nombre: string;
    apellido: string;
    fecha_nacimiento: Date;
    username: string;
    password: string;
    identificacion: number;
    codigo_tipo_identificacion: number;
    codigo_estado: number;
}
