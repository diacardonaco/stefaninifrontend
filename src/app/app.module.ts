import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ConsultarPersonasComponent } from './consultar-personas/consultar-personas.component';
import { NavbarComponent } from './comun/navbar/navbar.component';
import { LoginComponent } from './comun/login/login.component';
import { CrearPersonaComponent } from './crear-persona/crear-persona.component';
import { AppRoutingModule } from './app-routing.module';

import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ConsultarPersonasComponent,
    NavbarComponent,
    LoginComponent,
    CrearPersonaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
