import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,Routes } from '@angular/router';
import {ConsultarPersonasComponent} from '../app/consultar-personas/consultar-personas.component';
import {CrearPersonaComponent} from '../app/crear-persona/crear-persona.component'

const appRoutes: Routes = [
    { path: 'personas', component: ConsultarPersonasComponent},
    { path: 'crearpersona', component: CrearPersonaComponent}
  ];



@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
